/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bvalero <bvalero@student.42lyon.fr>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/13 00:43:18 by bvalero           #+#    #+#             */
/*   Updated: 2021/01/13 00:53:39 by bvalero          ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memcpy(void *s, const void *s2, size_t n)
{
	unsigned char		*s3;
	const unsigned char	*s4;
	size_t				cnt;

	s3 = (unsigned char *)s;
	s4 = (const unsigned char *)s2;
	if (s3 == s4)
		return (s);
	cnt = 0;
	while (cnt < n)
	{
		s3[cnt] = s4[cnt];
		++cnt;
	}
	return (s);
}
