/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bvalero <bvalero@student.42lyon.fr>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/15 06:16:28 by bvalero           #+#    #+#             */
/*   Updated: 2021/01/17 19:53:19 by bvalero          ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>

#include "libft.h"

static size_t	ft_split_count(const char *s, char c)
{
	bool	on_substr;
	size_t	cnt;
	size_t	cnt2;

	on_substr = false;
	cnt = 0;
	cnt2 = 0;
	while (s[cnt2] != '\0')
	{
		if (s[cnt2] == c)
			on_substr = false;
		else if (!on_substr)
		{
			++cnt;
			on_substr = true;
		}
		++cnt2;
	}
	return (cnt);
}

static void		ft_split_free(char **split, size_t cnt)
{
	size_t	cnt2;

	cnt2 = 0;
	while (cnt2 < cnt)
	{
		free(split[cnt2]);
		++cnt2;
	}
	free(split);
}

static char		**ft_split_fill(char **split, size_t cnt, const char *s, char c)
{
	size_t	substr_len;
	size_t	cnt2;
	size_t	cnt3;

	cnt2 = 0;
	cnt3 = 0;
	while (cnt2 < cnt)
	{
		while (s[cnt3] == c)
			++cnt3;
		substr_len = 0;
		while (s[cnt3 + substr_len] != c && s[cnt3 + substr_len] != '\0')
			++substr_len;
		if ((split[cnt2] = ft_substr(s, cnt3, substr_len)) == NULL)
		{
			ft_split_free(split, cnt2);
			return (NULL);
		}
		cnt3 += substr_len;
		++cnt2;
	}
	return (split);
}

char			**ft_split(const char *s, char c)
{
	char	**split;
	size_t	cnt;

	cnt = ft_split_count(s, c);
	if ((split = malloc((cnt + 1) * sizeof(*split))) == NULL)
		return (NULL);
	split[cnt] = NULL;
	split = ft_split_fill(split, cnt, s, c);
	return (split);
}
