/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bvalero <bvalero@student.42lyon.fr>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/15 03:09:49 by bvalero           #+#    #+#             */
/*   Updated: 2021/01/17 16:34:00 by bvalero          ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <stdbool.h>
#include <stddef.h>

#include "libft.h"

static bool	ft_is_in_charset(const char c, const char *set)
{
	size_t	cnt;

	cnt = 0;
	while (set[cnt] != '\0')
	{
		if (set[cnt] == c)
			return (true);
		++cnt;
	}
	return (false);
}

char		*ft_strtrim(const char *s, const char *set)
{
	size_t	start;
	size_t	end;

	start = 0;
	while (ft_is_in_charset(s[start], set))
		++start;
	if (s[start] == '\0')
		return (ft_calloc(1, sizeof(*s)));
	end = ft_strlen(s) - 1;
	while (ft_is_in_charset(s[end], set))
		--end;
	return (ft_substr(s, start, end - start + 1));
}
