/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_substr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bvalero <bvalero@student.42lyon.fr>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/15 02:05:24 by bvalero           #+#    #+#             */
/*   Updated: 2021/01/20 21:16:28 by bvalero          ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

#include "libft.h"

char	*ft_substr(const char *s, unsigned int start, size_t len)
{
	char	*s2;
	size_t	s_len;
	size_t	cnt;

	s_len = 0;
	while (s_len < start + len && s[s_len] != '\0')
		++s_len;
	if (s_len <= start)
		return (ft_calloc(1, sizeof(*s)));
	if (len > s_len - start)
		len = s_len - start;
	if ((s2 = malloc((len + 1) * sizeof(*s2))) == NULL)
		return (NULL);
	cnt = 0;
	while (cnt < len)
	{
		s2[cnt] = s[start + cnt];
		++cnt;
	}
	s2[cnt] = '\0';
	return (s2);
}
