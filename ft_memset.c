/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memset.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bvalero <bvalero@student.42lyon.fr>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/13 00:33:38 by bvalero           #+#    #+#             */
/*   Updated: 2021/01/13 00:40:12 by bvalero          ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memset(void *s, int c, size_t n)
{
	unsigned char	*s2;
	unsigned char	c2;
	size_t			cnt;

	s2 = (unsigned char *)s;
	c2 = (unsigned char)c;
	cnt = 0;
	while (cnt < n)
	{
		s2[cnt] = c2;
		++cnt;
	}
	return (s);
}
