/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bvalero <bvalero@student.42lyon.fr>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/13 03:46:53 by bvalero           #+#    #+#             */
/*   Updated: 2021/01/13 03:49:18 by bvalero          ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memchr(const void *s, int c, size_t n)
{
	unsigned char	*s2;
	unsigned char	c2;
	size_t			cnt;

	s2 = (unsigned char *)s;
	c2 = (unsigned char)c;
	cnt = 0;
	while (cnt < n)
	{
		if (s2[cnt] == c2)
			return (s2 + cnt);
		++cnt;
	}
	return (NULL);
}
