/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncmp.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bvalero <bvalero@student.42lyon.fr>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/13 13:06:11 by bvalero           #+#    #+#             */
/*   Updated: 2021/01/16 18:14:27 by bvalero          ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_strncmp(const char *s, const char *s2, size_t n)
{
	size_t	cnt;

	if (n == 0)
		return (0);
	cnt = 0;
	while (cnt < n - 1 && s[cnt] != '\0' && s[cnt] == s2[cnt])
		++cnt;
	return ((const unsigned char)s[cnt] - (const unsigned char)s2[cnt]);
}
