/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bvalero <bvalero@student.42lyon.fr>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/13 08:47:41 by bvalero           #+#    #+#             */
/*   Updated: 2021/01/17 16:19:10 by bvalero          ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>

#include "libft.h"

char	*ft_strchr(const char *s, int c)
{
	char	*s2;
	char	c2;
	size_t	cnt;

	s2 = (char *)s;
	c2 = (char)c;
	cnt = 0;
	while (s2[cnt] != c2)
	{
		if (s2[cnt] == '\0')
			return (NULL);
		++cnt;
	}
	return (s2 + cnt);
}
