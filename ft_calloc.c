/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_calloc.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bvalero <bvalero@student.42lyon.fr>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/15 01:22:37 by bvalero           #+#    #+#             */
/*   Updated: 2021/01/15 01:41:01 by bvalero          ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

#include "libft.h"

void	*ft_calloc(size_t n, size_t n2)
{
	size_t	n3;
	void	*p;

	n3 = n * n2;
	if ((p = malloc(n3)) != NULL)
		ft_memset(p, '\0', n3);
	return (p);
}
