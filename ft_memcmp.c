/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bvalero <bvalero@student.42lyon.fr>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/13 03:53:13 by bvalero           #+#    #+#             */
/*   Updated: 2021/01/13 04:16:38 by bvalero          ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_memcmp(const void *s, const void *s2, size_t n)
{
	const unsigned char	*s3;
	const unsigned char *s4;
	size_t				cnt;

	s3 = (const unsigned char *)s;
	s4 = (const unsigned char *)s2;
	if (n == 0)
		return (0);
	cnt = 0;
	while (cnt < n - 1 && s3[cnt] == s4[cnt])
		++cnt;
	return (s3[cnt] - s4[cnt]);
}
