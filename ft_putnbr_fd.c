/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_fd.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bvalero <bvalero@student.42lyon.fr>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/16 08:48:52 by bvalero           #+#    #+#             */
/*   Updated: 2021/01/17 16:39:50 by bvalero          ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <limits.h>
#include <stdbool.h>
#include <stddef.h>

#include <unistd.h>

#include "libft.h"

void	ft_putnbr_fd(int n, int fd)
{
	char	buf[sizeof(n) * CHAR_BIT + 1];
	int		sign;
	size_t	cnt;

	if (n < 0)
		sign = -1;
	else
	{
		sign = 1;
		n = -n;
	}
	cnt = sizeof(buf) - 1;
	while (true)
	{
		buf[cnt] = -(n % 10) + '0';
		n /= 10;
		--cnt;
		if (n == 0)
			break ;
	}
	if (sign == -1)
		buf[cnt] = '-';
	else
		++cnt;
	write(fd, buf + cnt, (sizeof(buf) - cnt) * sizeof(*buf));
}
