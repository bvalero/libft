/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bvalero <bvalero@student.42lyon.fr>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/16 06:48:12 by bvalero           #+#    #+#             */
/*   Updated: 2021/01/17 16:37:49 by bvalero          ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>
#include <stdlib.h>

#include "libft.h"

char	*ft_strmapi(const char *s, char (*f)(unsigned int, char))
{
	char	*s2;
	size_t	s_len;
	size_t	cnt;

	s_len = ft_strlen(s);
	if ((s2 = malloc((s_len + 1) * sizeof(*s2))) == NULL)
		return (NULL);
	cnt = 0;
	while (cnt < s_len)
	{
		s2[cnt] = f(cnt, s[cnt]);
		++cnt;
	}
	s2[cnt] = '\0';
	return (s2);
}
