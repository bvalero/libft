/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bvalero <bvalero@student.42lyon.fr>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/13 02:50:17 by bvalero           #+#    #+#             */
/*   Updated: 2021/01/16 18:01:20 by bvalero          ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memmove(void *s, const void *s2, size_t n)
{
	unsigned char		*s3;
	const unsigned char	*s4;
	size_t				cnt;

	s3 = (unsigned char *)s;
	s4 = (const unsigned char *)s2;
	if (s3 == s4)
		return (s);
	if (s3 < s4)
		return (ft_memcpy(s3, s4, n));
	else
	{
		cnt = n;
		while (cnt > 0)
		{
			--cnt;
			s3[cnt] = s4[cnt];
		}
	}
	return (s);
}
