/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bvalero <bvalero@student.42lyon.fr>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/13 13:33:05 by bvalero           #+#    #+#             */
/*   Updated: 2021/01/14 04:23:41 by bvalero          ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_strlcpy(char *s, const char *s2, size_t n)
{
	size_t	cnt;

	cnt = 0;
	if (n != 0)
	{
		while (cnt < n - 1 && s2[cnt] != '\0')
		{
			s[cnt] = s2[cnt];
			++cnt;
		}
		s[cnt] = '\0';
	}
	while (s2[cnt] != '\0')
		++cnt;
	return (cnt);
}
