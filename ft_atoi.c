/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bvalero <bvalero@student.42lyon.fr>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/14 05:13:13 by bvalero           #+#    #+#             */
/*   Updated: 2021/01/17 16:25:01 by bvalero          ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>

#include "libft.h"

static int	ft_isspace(int c)
{
	return ((c >= 0x9 && c <= 0xD) || c == ' ');
}

int			ft_atoi(const char *s)
{
	int		n;
	char	sign;
	size_t	cnt;

	n = 0;
	cnt = 0;
	while (ft_isspace((unsigned char)s[cnt]))
		++cnt;
	sign = s[cnt];
	if (sign == '+' || sign == '-')
		++cnt;
	while (ft_isdigit((unsigned char)s[cnt]))
	{
		n = n * 10 - (s[cnt] - '0');
		++cnt;
	}
	if (sign != '-')
		n = -n;
	return (n);
}
