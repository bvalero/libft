/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bvalero <bvalero@student.42lyon.fr>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/13 09:01:47 by bvalero           #+#    #+#             */
/*   Updated: 2021/01/17 16:19:29 by bvalero          ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <stdbool.h>
#include <stddef.h>

#include "libft.h"

char	*ft_strrchr(const char *s, int c)
{
	char	*s2;
	char	c2;
	char	*last_oc;
	size_t	cnt;

	s2 = (char *)s;
	c2 = (char)c;
	last_oc = NULL;
	cnt = 0;
	while (true)
	{
		if (s2[cnt] == c2)
			last_oc = s2 + cnt;
		if (s2[cnt] == '\0')
			break ;
		++cnt;
	}
	return (last_oc);
}
