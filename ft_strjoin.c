/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bvalero <bvalero@student.42lyon.fr>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/15 02:50:04 by bvalero           #+#    #+#             */
/*   Updated: 2021/01/17 19:27:41 by bvalero          ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>
#include <stdlib.h>

#include "libft.h"

char	*ft_strjoin(const char *s, const char *s2)
{
	char	*s3;
	size_t	s_len;
	size_t	s2_len;
	size_t	cnt;
	size_t	cnt2;

	s_len = ft_strlen(s);
	s2_len = ft_strlen(s2);
	if ((s3 = malloc((s_len + s2_len + 1) * sizeof(*s3))) == NULL)
		return (NULL);
	cnt = 0;
	while (cnt < s_len)
	{
		s3[cnt] = s[cnt];
		++cnt;
	}
	cnt2 = 0;
	while (cnt2 < s2_len)
	{
		s3[cnt + cnt2] = s2[cnt2];
		++cnt2;
	}
	s3[cnt + cnt2] = '\0';
	return (s3);
}
