/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstadd_back.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bvalero <bvalero@student.42lyon.fr>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/16 14:15:36 by bvalero           #+#    #+#             */
/*   Updated: 2021/01/16 17:22:49 by bvalero          ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstadd_back(t_list **lst, t_list *e)
{
	t_list	*lst2;

	lst2 = *lst;
	if (lst2 == NULL)
		*lst = e;
	else
	{
		lst2 = ft_lstlast(lst2);
		lst2->next = e;
	}
}
