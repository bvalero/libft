/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bvalero <bvalero@student.42lyon.fr>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/13 22:04:24 by bvalero           #+#    #+#             */
/*   Updated: 2021/01/14 04:21:59 by bvalero          ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_strlcat(char *s, const char *s2, size_t n)
{
	size_t	s_len;
	size_t	cnt;

	s_len = 0;
	while (s_len < n && s[s_len] != '\0')
		++s_len;
	if (n - s_len == 0)
		return (ft_strlen(s2) + s_len);
	cnt = 0;
	while (cnt < n - s_len - 1 && s2[cnt] != '\0')
	{
		s[s_len + cnt] = s2[cnt];
		++cnt;
	}
	s[s_len + cnt] = '\0';
	while (s2[cnt] != '\0')
		++cnt;
	return (cnt + s_len);
}
