/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstclear.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bvalero <bvalero@student.42lyon.fr>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/16 14:35:38 by bvalero           #+#    #+#             */
/*   Updated: 2021/01/16 15:30:57 by bvalero          ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstclear(t_list **lst, void (*del)(void *))
{
	t_list	*lst2;
	t_list	*tmp;

	lst2 = *lst;
	while (lst2 != NULL)
	{
		tmp = lst2;
		lst2 = lst2->next;
		ft_lstdelone(tmp, del);
	}
	*lst = NULL;
}
