/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bvalero <bvalero@student.42lyon.fr>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/15 01:22:55 by bvalero           #+#    #+#             */
/*   Updated: 2021/01/16 18:36:14 by bvalero          ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

#include "libft.h"

char	*ft_strdup(const char *s)
{
	char	*s2;
	size_t	n;

	n = (ft_strlen(s) + 1) * sizeof(*s);
	if ((s2 = malloc(n)) == NULL)
		return (NULL);
	ft_memcpy(s2, s, n);
	return (s2);
}
