/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bvalero <bvalero@student.42lyon.fr>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/14 02:21:51 by bvalero           #+#    #+#             */
/*   Updated: 2021/01/20 11:21:52 by bvalero          ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnstr(const char *s, const char *s2, size_t n)
{
	char	*s3;
	size_t	s2_len;
	size_t	cnt;
	size_t	cnt2;

	s3 = (char *)s;
	s2_len = ft_strlen(s2);
	if (s2_len == 0)
		return (s3);
	if (s2_len > n)
		return (NULL);
	cnt = 0;
	while (cnt < n - s2_len + 1)
	{
		cnt2 = 0;
		while (cnt2 < s2_len && s3[cnt + cnt2] != '\0'
				&& s2[cnt2] == s3[cnt + cnt2])
			++cnt2;
		if (cnt2 == s2_len)
			return (s3 + cnt);
		if (s3[cnt + cnt2] == '\0')
			return (NULL);
		++cnt;
	}
	return (NULL);
}
