/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bvalero <bvalero@student.42lyon.fr>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/16 14:49:30 by bvalero           #+#    #+#             */
/*   Updated: 2021/01/16 18:58:06 by bvalero          ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstmap(t_list *lst, void *(*f)(void *), void (*del)(void *))
{
	t_list	*lst2;
	t_list	*e;
	t_list	*tmp;

	lst2 = NULL;
	while (lst != NULL)
	{
		if ((e = ft_lstnew(f(lst->content))) == NULL)
		{
			ft_lstclear(&lst2, del);
			break ;
		}
		if (lst2 == NULL)
			lst2 = e;
		else
			tmp->next = e;
		tmp = e;
		lst = lst->next;
	}
	return (lst2);
}
