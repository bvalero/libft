/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bvalero <bvalero@student.42lyon.fr>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/13 02:19:33 by bvalero           #+#    #+#             */
/*   Updated: 2021/01/13 02:46:30 by bvalero          ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memccpy(void *s, const void *s2, int c, size_t n)
{
	unsigned char		*s3;
	const unsigned char	*s4;
	unsigned char		c2;
	size_t				cnt;

	s3 = (unsigned char *)s;
	s4 = (const unsigned char *)s2;
	c2 = (unsigned char)c;
	cnt = 0;
	while (cnt < n)
	{
		if ((s3[cnt] = s4[cnt]) == c2)
			return (s3 + cnt + 1);
		++cnt;
	}
	return (NULL);
}
