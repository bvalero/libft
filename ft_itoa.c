/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bvalero <bvalero@student.42lyon.fr>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/15 15:50:53 by bvalero           #+#    #+#             */
/*   Updated: 2021/01/20 21:38:08 by bvalero          ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <limits.h>
#include <stdbool.h>
#include <stddef.h>

#include "libft.h"

char	*ft_itoa(int n)
{
	char	buf[sizeof(n) * CHAR_BIT + 1];
	int		sign;
	size_t	cnt;

	if (n < 0)
		sign = -1;
	else
	{
		sign = 1;
		n = -n;
	}
	cnt = sizeof(buf) - 1;
	while (true)
	{
		buf[cnt] = -(n % 10) + '0';
		n /= 10;
		--cnt;
		if (n == 0)
			break ;
	}
	if (sign == -1)
		buf[cnt] = '-';
	else
		++cnt;
	return (ft_substr(buf + cnt, 0, sizeof(buf) - cnt));
}
